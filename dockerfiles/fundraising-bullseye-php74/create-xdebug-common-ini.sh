#!/bin/bash

# Dynamically create config for xdebug, so we can include the current IP
# address of the host machine as seen from the container (since Docker does not
# make this easily discoverable using DNS).

host_ip=$(route -n | grep 'UG[ \t]' | awk '{print $2}')

cat << EOF > /srv/config/internal/xdebug-common.ini
xdebug.log=/tmp/xdebug.log
xdebug.mode=develop,debug
xdebug.client_host=${host_ip}
xdebug.client_port=9000
EOF
