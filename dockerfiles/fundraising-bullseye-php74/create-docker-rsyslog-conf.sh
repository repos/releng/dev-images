#!/bin/bash

# We need to run rsyslog to capture syslog and xdebug.log and send them to the logger container.
# We re-create the configuration file each time to use the environment variables to see which
# user/group to switch to.
# NOTE: Coordinate port 9514 with config provided to logger container

cat << EOF > /srv/config/internal/docker-rsyslog.conf
module(load="imuxsock")
module(load="imfile")

input(type="imfile"
     File="/tmp/xdebug.log"
     Tag="xdebug:"
     Facility="local3"
)

\$PrivDropToUserID ${FR_DOCKER_UID}
\$PrivDropToGroupID ${FR_DOCKER_GID}

*.* @${FR_DOCKER_LOGGER_HOST}:9514}
EOF