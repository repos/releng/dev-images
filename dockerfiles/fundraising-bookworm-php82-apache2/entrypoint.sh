#!/bin/bash

# Create rsyslog conf and common xdebug conf
/srv/create-docker-rsyslog-conf.sh
/srv/create-xdebug-common-ini.sh

# /srv/config/exposed appears on the host as config. The files we link to
# below should be provided by the host. (In the case of fundraising-dev, they
# are written by setup.sh or provided as part of the fundraising-dev git repo.)

# These links are created here so we can use environment variables in them.

ln -sf /srv/config/exposed/${FR_DOCKER_SERVICE_NAME}/xdebug-web.ini /etc/php/8.2/apache2/conf.d/30-xdebug-web.ini
ln -sf /srv/config/exposed/${FR_DOCKER_SERVICE_NAME}/xdebug-cli.ini /etc/php/8.2/cli/conf.d/30-xdebug-cli.ini
ln -sf /srv/config/exposed/${FR_DOCKER_SERVICE_NAME}/000-default.conf /etc/apache2/sites-enabled/000-default.conf

# -T option required because links upon links to a directory
ln -sfT /srv/config/exposed/${FR_DOCKER_SMASHPIG_CONFIG_DIR} /srv/config/internal/smashpig

# In case an old apache pid file is still there (location set in envars)
rm -f /tmp/apache2/apache2.pid

/usr/sbin/rsyslogd -f /srv/config/internal/docker-rsyslog.conf -iNONE && \
	/usr/sbin/apache2ctl -D FOREGROUND
