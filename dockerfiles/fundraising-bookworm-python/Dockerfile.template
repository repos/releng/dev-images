# Base Python image FR-Tech development environment.

FROM {{ "fundraising-bookworm" | image_tag }} AS bookworm

{% set packages|replace('\n', ' ') -%}
python3-pip
python3-dateutil
default-libmysqlclient-dev
tox
gcc
python3-dev
python3-venv
python3-wheel
cron
rsyslog
net-tools
host
nano
vim
less
{%- endset -%}

RUN {{ packages | apt_install }}

ENV LOGNAME=docker

# Python debugging
EXPOSE 5678

# Config directories
RUN mkdir -p /srv/config/exposed \
    && mkdir -p /srv/config/private \
    && mkdir -p /srv/config/internal \
    && chmod -R a+rw /srv/config/* \
    # Home directory for docker user. This is actually a "mock" home, since we never
    # create this user, and containers run under the local host user's uid and gid.
    && mkdir /home/docker \
    && chmod a+rw /home/docker

# Make a nice bash prompt
COPY bashrc_customize_prompt /srv/bashrc_customize_prompt
RUN cat /srv/bashrc_customize_prompt >> /etc/bash.bashrc

# Set $HOME to fake home
ENV HOME="/home/docker"

# Default values for uid/gid (normally overridden in docker-compose.yml)
ENV FR_DOCKER_UID=1000
ENV FR_DOCKER_GID=1000

