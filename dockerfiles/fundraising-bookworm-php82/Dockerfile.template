# Base PHP image FR-Tech development environment.
# Provides PHP and other common tools for images for FR-Tech Mediawiki and Civicrm.

FROM {{ "fundraising-bookworm" | image_tag }} AS bookworm

# Note that WMF APT key is pre-installed.
# See: https://wikitech.wikimedia.org/wiki/APT_repository#Security

# Install debian packages

# netcat and host for debugging connections to other containers in the application.
# net-tools for finding host IP address as visible from within container the container.

# FIXME Separate out packages only needed by Civicrm or Mediawiki into child images
# Currently includes packages recommended for Civicrm here:
# https://github.com/eileenmcnaughton/civicrm-buildkit-docker/blob/d8304ea127218c1a3b522a476399c9a0d3774a7b/civicrm/Dockerfile#L31
# TODO Also double-check which of those packages we really need

{% set packages|replace('\n', ' ') -%}
php-apcu
php-bcmath
php-cli
php-curl
php-gd
php-gmp
php-intl
php-ldap
php-mbstring
php-mysql
php-pgsql
php-sqlite3
php-xml
php-zip
php-redis
php-opcache
php-soap
php-xdebug
jq
curl
nodejs
npm
git
libc-client-dev
libicu-dev
libjpeg62-turbo-dev
libkrb5-dev
libmagickwand-dev
libpng-dev
libxml2-dev
mariadb-client
nodejs
unzip
zip
bzip2
libzip-dev
rsyslog
net-tools
host
netcat-traditional
nano
vim
less
{%- endset -%}

RUN {{ packages | apt_install }}

# Install composer

RUN mkdir /srv/composer
COPY composer.phar.sha256sum /srv/composer/composer.phar.sha256sum
RUN cd /srv/composer \
    && curl --silent --fail --output composer.phar https://getcomposer.org/download/2.7.1/composer.phar \
    && sha256sum -c composer.phar.sha256sum \
    && chmod +x /srv/composer/composer.phar \
    && mv /srv/composer/composer.phar /usr/bin/composer

# Config directories
RUN mkdir -p /srv/config/exposed \
    && mkdir -p /srv/config/private \
    && mkdir -p /srv/config/internal \
    && chmod -R a+rw /srv/config/* \
    # Home directory for docker user. This is actually a "mock" home, since we never
    # create this user, and containers run under the local host user's uid and gid.
    && mkdir /home/docker \
    && chmod a+rw /home/docker

# Make a nice bash prompt
COPY bashrc_customize_prompt /srv/bashrc_customize_prompt
RUN cat /srv/bashrc_customize_prompt >> /etc/bash.bashrc

# Set $HOME to fake home
ENV HOME="/home/docker"

# Common bash scripts used by entrypoints in child images
COPY create-docker-rsyslog-conf.sh /srv/
COPY create-xdebug-common-ini.sh /srv/

# Copy in ssl cert and key
COPY ./docker-ssl.pem /etc/ssl/certs/
COPY ./docker-ssl.key /etc/ssl/private/

# Set some permissions required for the image to run as non-root
RUN chmod a+xr /etc/ssl/private/ \
    && chmod u+s `realpath /usr/sbin/rsyslogd` \
	# Make scripts used by entrypoints executable
    && chmod a+x /srv/create-docker-rsyslog-conf.sh /srv/create-xdebug-common-ini.sh

# Default values for uid/gid (normally overridden in docker-compose.yml)
ENV FR_DOCKER_UID=1000
ENV FR_DOCKER_GID=1000

# No entrypoint, this is just a base image for other images.
