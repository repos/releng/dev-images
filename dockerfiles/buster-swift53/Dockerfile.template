# The build image:
# - downloads Swift tarball and the gpg signature
# - verify the gpg signature
# - extract the content and move it to /opt/swift
FROM {{ "buster" | image_tag }} AS downloader

RUN {{ "gnupg wget" | apt_install }}

# Swift release key https://swift.org/keys/all-keys.asc
COPY all-keys.asc /tmp/org.swift.asc

RUN set -x \
    && curl -fsSL \
        -O https://download.swift.org/swift-5.3-release/ubuntu1804/swift-5.3-RELEASE/swift-5.3-RELEASE-ubuntu18.04.tar.gz \
        -O https://download.swift.org/swift-5.3-release/ubuntu1804/swift-5.3-RELEASE/swift-5.3-RELEASE-ubuntu18.04.tar.gz.sig

RUN set -x && gpg --batch --import /tmp/org.swift.asc \
    && rm /tmp/org.swift.asc \
    && gpg --verify \
        swift-5.3-RELEASE-ubuntu18.04.tar.gz.sig \
        swift-5.3-RELEASE-ubuntu18.04.tar.gz \
    && rm -rf ~/.gnupg /tmp/org.swift.asc \
    && tar -C opt -xzf swift-5.3-RELEASE-ubuntu18.04.tar.gz \
    && mv /opt/swift* /opt/swift \
    && rm \
        swift-5.3-RELEASE-ubuntu18.04.tar.gz \
        swift-5.3-RELEASE-ubuntu18.04.tar.gz.sig

# The published image
FROM {{ "buster" | image_tag }} AS buster

{% set dev_deps|replace('\n', ' ') -%}
binutils
libc6-dev
libcurl4
libedit2
libgcc-8-dev
libncurses5
libpython2.7
libsqlite3-0
libstdc++-8-dev
libtinfo5
libxml2
pkg-config
tzdata
zlib1g-dev
{%- endset -%}

RUN {{ dev_deps | apt_install }}

COPY --from=downloader /opt/swift /opt/swift

ENV PATH="/opt/swift/usr/bin:${PATH}"
